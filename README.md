# The GPG keys of our key maintainers used to sign packages in our repo(s)

## >[[NOTE!]]

This should never need to be manually installed by the end user. Pacman will install this when the system is set up, and it will be automatically kept up-to-date alongside normal packages.
